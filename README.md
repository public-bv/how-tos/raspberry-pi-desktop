# BitsVital Raspberry Pi Desktop How To's

- [BitsVital Raspberry Pi Desktop How To's](#bitsvital-raspberry-pi-desktop-how-tos)
  - [Installing Programs](#installing-programs)
    - [How To Install FireFox](#how-to-install-firefox)
    - [How To Install Microsoft VSCode IDE](#how-to-install-microsoft-vscode-ide)


## Installing Programs

### How To Install FireFox
The quickest and easiest way to install FireFox Browser is using the terminal.

1. Open the terminal

```
sudo apt update
sudo apt install firefox-esr
```

This will update the package list, upgrade existing packages, and install the Firefox ESR (Extended Support Release) version. Once the installation is complete, you will find Firefox Browser in the "Internet" section of the applications menu.

### How To Install Microsoft VSCode IDE

The quickest and easiest way to install Microsoft VSCode IDE is using the terminal.

1. Open the terminal

```
sudo apt update
sudo apt install code
```

After the installation is complete, you can launch VSCode from the "Programming" section of the applications menu.

**BitsVital Copyright 2023 All Rights Reserved**
